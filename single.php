<?php get_header(); ?>
    <main class="container grid-xl single_post_main">
        <div class="columns">
            <div class="column col-12">
                <?php
                // Start the loop.

                while ( have_posts() ) : the_post(); ?>
                    <div class="single_post_main__inner <?php echo (!comments_open()) ? 'border_b' : ''; ?>">

                    <?php
                        get_template_part( 'template-parts/content', get_post_format() );
                    ?>
                    </div>
                    <?php
                    // If comments are open or we have at least one comment, load up the comment template.
                    ?>
                    <?php
                    if ( comments_open() ) :
                        echo '<div class="comments_cnt">
                                <div class="to_animate">';
                            comments_template();
                        echo '</div>
                            </div>';
                    endif;
                    
                // End the loop.
                endwhile;
                ?>
            </div>
        </div>
    </main><!-- .site-main -->
<?php get_footer(); ?>