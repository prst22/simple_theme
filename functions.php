<?php
function theme_setup() {
	load_theme_textdomain( 'simple-theme' );

	// Adding menus to wp controll pannel
	register_nav_menus(array(
		'main_menu' =>__('Main menu'),
		'footer_menu' =>__('Footer Menu')
	));

    //post thum pictures
    add_theme_support('post-thumbnails');

    add_image_size('small-thumbnail', 180, 101, array('center','center'));
    add_image_size('medium-thumbnail', 577, 325, array('center','center'));  
    add_image_size('large-thumnbail', 1200, 675, array('center','center'));
   
    add_theme_support( 'post-formats', array( 'video', 'link', 'quote' ));

	//CUSTOM WORDPRESS EDITOR STYLE
	add_editor_style( 'css/custom-editor-styles.min.css' );
    //CUSTOM WORDPRESS EDITOR STYLE 
    add_theme_support( 'customize-selective-refresh-widgets' );

}
add_action( 'after_setup_theme', 'theme_setup' );

//add theme support end


//adding css styles start
function add_styles_js(){
	wp_enqueue_style('style', get_theme_file_uri( '/css/main.min.css'));
	wp_enqueue_script('main_js', get_template_directory_uri() . '/js/main.min.js', array(), 1.0, true);
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action('wp_enqueue_scripts', 'add_styles_js');
//adding css styles end

//Controll post length on the main page

function set_excerpt_length(){
	if(get_post_format() == 'video'){
		return 15;
	}else{
		return has_post_thumbnail() ? 15 : 30;
	}
}
add_filter('excerpt_length', 'set_excerpt_length');

function cas_theme_custom_excerpt() {
    return ' ...';
}
add_filter( 'excerpt_more', 'cas_theme_custom_excerpt' );

// adding class names to previous and next pagination links of get_next/previous_posts_link start

add_filter('next_posts_link_attributes', 'posts_link_attributes');
add_filter('previous_posts_link_attributes', 'posts_link_attributes');

function posts_link_attributes() {
	return (current_filter() == 'next_posts_link_attributes') ? 'class="btn next_post_link"' : 'class="btn previous_post_link"';
}
// adding class names to previous and next pagination links of get_next/previous_posts_link end

// responsive embed videos start

function embed_html( $html ) {
   return '<div class="responsive_content"><div class="video_content">' . $html . '</div></div>';
}
add_filter( 'embed_oembed_html', 'embed_html', 10, 3 );

// responsive embed videos end

// modify quotes post format content start
add_filter('the_content','quote_post_edit_content');

function quote_post_edit_content($content){
	if( get_post_format( get_the_ID() ) == 'quote' ){
		$cnt_modifyed = strip_tags($content, '<a><span><b><i>');
		$content = '<p><span class="pr-2"><i class="fas fa-quote-right"></i></span>'
		. $cnt_modifyed  . 
		'<span class="pl-2"> <i class="fas fa-quote-left"></i></span></p>';
		return $content;
	}else{
		return $content;
	}
}
// modify quotes post format content end

// removing url field in comments section start
function disable_url_in_comments_field($fields){
	if(isset($fields['url']))
	unset($fields['url']);
	return $fields;
}
add_filter('comment_form_default_fields', 'disable_url_in_comments_field');
// removing url field in comments section end

function rkk_disable_admin_bar() {
    if( ! current_user_can('edit_posts') )
        add_filter('show_admin_bar', '__return_false');
}
add_action( 'after_setup_theme', 'rkk_disable_admin_bar' );

function wpse23007_redirect(){
  if( is_admin() && !defined('DOING_AJAX') && ( current_user_can('subscriber') || current_user_can('contributor') ) ){
    wp_redirect(home_url());
    exit;
  }
}
add_action('init','wpse23007_redirect');

function tu_comment_form_change_cookies_consent( $fields ) {
	$commenter = wp_get_current_commenter();

	$consent   = empty( $commenter['comment_author_email'] ) ? '' : ' checked="checked"';

	$fields['cookies'] = '<p class="comment-form-cookies-consent form-group">
		<label for="wp-comment-cookies-consent" class="form-checkbox">Save my name, email in this browser for the next time I comment.
		<input id="wp-comment-cookies-consent" name="wp-comment-cookies-consent" type="checkbox" value="yes"' . $consent . ' />' . '<i class="form-icon"></i>
		</label></p>';
	return $fields;
}
add_filter( 'comment_form_default_fields', 'tu_comment_form_change_cookies_consent' );

// style reply link in commets start

function replace_reply_link_class($class){
    $class = str_replace("class='comment-reply-link", "class='comment-reply-link btn btn-sm", $class);
    return $class;
}

add_filter('comment_reply_link', 'replace_reply_link_class');
// style reply link in commets end

//disable plugins from update start
if ( class_exists( 'UsersWP' ) || class_exists( 'bbPress' ) ) {
	function simple_filter_plugin_updates( $value ) {
	    if( isset( $value->response['userswp/userswp.php'] ) ) {        
	    	unset( $value->response['userswp/userswp.php'] );
	    }
	    if( isset( $value->response['bbpress/bbpress.php'] ) ) {
	    	unset( $value->response['bbpress/bbpress.php'] );
	    }
	    return $value;
	}
	add_filter( 'site_transient_update_plugins', 'simple_filter_plugin_updates' );
}
//disable plugins from update end