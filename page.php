<?php get_header();?>
    <main class="container grid-xl main">
        <div class="columns">
            <div class="column col-12">
                <div class="single_page main__inner">
                    <?php while ( have_posts() ) : the_post(); ?>
                    <div class="single_page__inner to_animate">
                        <header class="single_page_heading">
                            <h1 class="mb-3 text-center heading_title"><?php the_title(); ?></h1>
                        </header>
                        <div class="content_here">
                            <?php  the_content();  ?>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </main>
<?php get_footer();?>