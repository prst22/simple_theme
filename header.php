<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="description" content="<?php bloginfo('description'); ?>">
	<title>
		<?php bloginfo('name');?> |
		<?php is_front_page() ? bloginfo('description') : wp_title('');?> 
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="Igor Barabash">
	<meta name="keywords" content="Custom wordpress theme">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/site.webmanifest">
	<link rel="mask-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<div class="overflow"><!--menu wrapper for blur effect -->

	<div class="site_wrap">
		<section class="container grid-xl">
			<div class="columns">
				<div class="column col-12">
					<div class="site_header_cnt">
						<header class="site_header_cnt__site_header">
							<a href="<?php echo home_url();?>" title="Go home" class="main_site_header">
								<img src="<?php echo esc_url(get_template_directory_uri() . '/images/logo-s.svg'); ?>" alt="<?php bloginfo('name'); ?>">
							</a>
							<?php
								$user_wp_profile_edit = get_site_url(null, '/your-account/profile/');
								$user = wp_get_current_user();
								$user_info = get_userdata($user->ID); 
                        		$forum_nicename = bbp_get_displayed_user_field( 'user_nicename' ); 
							 ?>
							<?php if(is_user_logged_in() && class_exists( 'UsersWP' )): ?>
							<div class="login_info">
								
								 <div>
								 	<span>
								 		<i class="fas fa-address-card"></i>
								 		<?php _e( 'Logged in as', 'simple-theme' ); ?>
								 	</span>
								 	<a href="<?php echo $user_wp_profile_edit; ?>">
								 		<b><?php echo $user_info->user_login; ?></b>
								 	</a>
								 </div>
								
							</div>
							<?php endif; ?>
						</header>
					</div>
					<?php if ( has_nav_menu( 'main_menu' ) ) : ?>

					<nav class="nav <?php echo ( get_bloginfo('name') !== '' ) ? 'nav--justify' : ''; ?>">
						<h3 class="nav__mobile_site_header">
							<a href="<?php echo home_url();?>" title="Go home">
								<?php bloginfo('name'); ?>
							</a>
						</h3>
						<div class="mobile_menu_toggle">
						    <div class="mobile_menu_toggle__button">
						    	<button class="btn" id="mobile_menu_btn">
									<i class="fas fa-ellipsis-h"></i>
								</button>
						    </div>
						</div>
						
						<?php
						    wp_nav_menu( array(
								   'menu'              => 'Primary menu',
								   'theme_location'    => 'primary_menu',
								   'container_class'   => 'nav__inner position-relative',
								   'menu_class'        => 'nav_list',
								   'depth'             => 1,                  
							   )
						   );
						?>
					</nav>
					<?php endif; ?>
				</div>
			</div>
		</section>
	
	
	