<?php
/*
Template Name: page 404
*/
?>
<?php
get_header(); ?>

<main class="container grid-xl main">
	<div class="columns">
		<div class="column col-12">
        	<div class="page_404 single_page main__inner to_animate">
        		<h1>Something went wrong 404</h1>
        	</div>
		</div>
	</div>
</main>

<?php get_footer();