<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 * @since 1.0
 * @version 1.0
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */

?>

<div id="comments" class="comments-area">
    <?php
    // You can start editing here -- including this comment!
    if ( have_comments() ) : ?>
        <h3 class="comments-title">
            <?php
            $comments_number = get_comments_number();
            if ( '1' === $comments_number ) {
                /* translators: %s: post title */
                printf( _x( 'One review on &ldquo;%s&rdquo;', 'comments title', 'simple_theme' ), get_the_title() );
            } else {
                printf(
                    /* translators: 1: number of comments, 2: post title */
                    _nx(
                        '%1$s commen on &ldquo;%2$s&rdquo;',
                        '%1$s comments on &ldquo;%2$s&rdquo;',
                        $comments_number,
                        'comments title',
                        'simple_theme'
                    ),
                    number_format_i18n( $comments_number ),
                    get_the_title()
                );
            }
            ?>
        </h3>

        <ul class="comment-list">
            <?php
                wp_list_comments( array(
                    'avatar_size' => 100,
                    'style'       => 'ul',
                    'short_ping'  => true,
                    'reply_text'  => 'Reply',
                    'format'      => 'html5'
                ) );
            ?>
        </ul>
        <?php 
            the_comments_pagination( array(
            'prev_text' => '<i class="fas fa-long-arrow-alt-left"></i>',
            'next_text' => '<i class="fas fa-long-arrow-alt-right"></i>',
        ) );
    endif; // Check for have_comments().

    // If comments are closed and there are comments, let's leave a little note, shall we?
    if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>

        <p class="no-comments"><?php _e( 'Comments are closed.', 'simple_theme' ); ?></p>

    <?php
    endif;
    $args = array('class_submit' => 'submit btn');
    comment_form($args);
    ?>

</div><!-- #comments -->
