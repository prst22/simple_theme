<?php
/* Template Name: Login template */
?>
<?php get_header();?>
	<main class="container grid-xl main">
	    <div class="columns">
			<div class="column col-12">
				<div class="single_page main__inner padding_b">
                    <?php while ( have_posts() ) : the_post(); ?>

                        <div class="content to_animate">
                            <?php  the_content();  ?>
                        </div>

                    <?php endwhile; ?>
                </div>
			</div>
		</div>
	</main>
<?php get_footer();?>