	<div class="container grid-xl">
		<div class="columns">
			<footer class="column col-12 footer">
				<div class="footer__inner">
					<?php  if( has_nav_menu( 'footer_menu' ) ): ?>

						<div class="footer_content">
							<div class="">
								<div class="">
									<?php 
									$icon_facebook = get_theme_mod('footer_social_one');
									$icon_twitter = get_theme_mod('footer_social_two');
									if (!empty($icon_facebook) || !empty($icon_twitter)): ?>
			                            <div class="footer_content__social">
										<?php if (get_theme_mod('footer_social_one') !== ''): ?>
											<a href="<?php echo get_theme_mod('footer_social_one'); ?>" title="Our facebook link">
												<i>
													<svg class="icon icon-facebook-official"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-facebook-official"></use></svg>
												</i>
											</a>
										<?php endif; ?>
										<?php if(get_theme_mod('footer_social_two') !== ''): ?>
											<a href="<?php echo get_theme_mod('footer_social_two'); ?>" title="Our twitter link">
												<i>
													<svg class="icon icon-twitter"><use xlink:href="<?php echo get_template_directory_uri(); ?>/symbol-defs.svg#icon-twitter"></use></svg>
												</i>
											</a>
										<?php endif; ?>
			                            </div>
		                            <?php endif; ?>
								</div>
								<div class="">
									<?php
										if(is_active_sidebar('footer_widget')){ 
								            dynamic_sidebar('footer_widget');
							            }
									?>
								</div>
							</div>

                        </div>
                    <?php endif; ?>
                    
		        </div>

				<div class="footer__bottom-copy">
					<span class="label label-rounded label-primary">
						<i class="far fa-copyright"></i>
						<?php echo date("Y") ;?> - <a href="http://www.potatosites.com/">potatosites.com</a>
					</span>	
				</div> 
				
			</footer>
		</div>
	</div>
	
</div>
</div><!--menu wrapper for blur effect end -->

<!-- mobile menu start -->
<?php if ( has_nav_menu( 'main_menu' ) ) : ?>
	<section class="mobile_menu_wrapper hidden">

		<nav class="mobile_menu_cnt hidden">
					<div class="mobile_menu_cnt__inner hidden">
						<div class="menu_content">
							<h2>MENU</h2>
			                <div class="close_menu_cnt">
			                	<button class="btn" id="close_menu_btn">
			                		<i class="fas fa-ellipsis-h"></i>
			                	</button>
								
			                </div>
							<?php
							   wp_nav_menu( array(
									   'menu'              => 'Primary menu',
									   'theme_location'    => 'primary_menu',
									   'container_class'   => 'mobile_menu_list_cnt',
									   'menu_class'        => 'mobile_menu_list_cnt__mobile_list',
									   'depth'             => 1,               
								   )
							   );
							?>
						</div>
					</div>
		</nav>

	</section> 
<?php endif; ?>
<!-- mobile menu end -->

<?php wp_footer(); ?>
</body>

</html>
