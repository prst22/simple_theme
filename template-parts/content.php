<article <?php post_class( array( 'single_post' ) ); ?>>

	<div>
		<header class="to_animate">
			<h1><?php the_title(); ?></h1>
		</header>
		<div class="post_info to_animate">
			<div class="categories">
				<?php
					$categories = get_the_category();
                    $divider = ', ';
					if ( ! empty( $categories ) ) {
						echo 'Category: ';
						foreach ($categories as $key => $cat) {
							if(count($categories) != ($key + 1 )){
								echo '<a href="' . esc_url( get_category_link( $cat->term_id ) ) . '">' . esc_html( $cat->name ) . '</a>'. $divider; 
							}else{
								echo '<a href="' . esc_url( get_category_link( $cat->term_id ) ) . '">' . esc_html( $cat->name ) . '</a>'; 
							}
						} 
					} 
				 ?>
			</div>
		</div>
		<?php if(has_post_thumbnail()): ?>
		    <div class="single_post_thumbnail_cnt">
			<?php the_post_thumbnail('small-thumbnail', 
				$attr = array(
					'class' => "single_post_thumbnail_cnt__img lazyload blur-up",
					'data-src'=> get_the_post_thumbnail_url( get_the_ID(), ' large-thumbnail'),
					'alt' => get_the_title(),
					'data-sizes' => 'auto'
				)); ?>
			</div>

		<?php endif;?>
		<div class="to_animate">
			<?php the_content(); ?>
		</div>
	</div>
	
</article>