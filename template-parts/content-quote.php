<article <?php post_class( array( 'single_post', 'quote' ) ); ?>>
	<div class="to_animate">
		<header>
			<h1><?php the_title(); ?></h1>
			<div class="post_info">
				<div class="categories">
					<?php
						$categories = get_the_category();
	                    $divider = ', ';
						if ( ! empty( $categories ) ) {
							echo 'Category: ';
							foreach ($categories as $key => $cat) {
								if(count($categories) != ($key + 1 )){
									echo '<a href="' . esc_url( get_category_link( $cat->term_id ) ) . '">' . esc_html( $cat->name ) . '</a>'. $divider; 
								}else{
									echo '<a href="' . esc_url( get_category_link( $cat->term_id ) ) . '">' . esc_html( $cat->name ) . '</a>'; 
								}
							} 
						} 
					 ?>
				</div>
			</div>
		</header>
		<div class="quote__content">
			<?php the_content(); ?>
		</div>
	</div>
	
</article>