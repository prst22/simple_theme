<article <?php post_class( array( 'single_post', 'single_link' ) ); ?>>

	<div class="to_animate">
		<h1><?php the_title(); ?></h1>
		<p>
			<a href="<?php echo wp_strip_all_tags(get_the_content()); ?>" class="single_link__link">
				<?php echo wp_strip_all_tags(get_the_content()); ?>
				<i class="pl-1 fas fa-external-link-alt"></i>
			</a>
		</p>
	</div>
	
</article>