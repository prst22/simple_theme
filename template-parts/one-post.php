<?php 
	$post_format = get_post_format();
	switch ($post_format ) {
    
		case 'video':?>
			<article <?php post_class( array('index_post', 'video_post', 'column', 'col-sm-12', 'col-lg-6', 'col-4') ); ?>>
				<div>
					<div class="">
						<?php if(has_post_thumbnail()): ?>
							<figure class="thumbnail_cnt video_thumbnail_cnt">
								<a href="<?php the_permalink() ?>" title="Link to <?php the_title_attribute(); ?>" class="thumbnail_cnt__link">
									<?php the_post_thumbnail('small-thumbnail', 
										$attr = array(
											'class' => "video_thumbnail lazyload blur-up",
											'data-src'=> get_the_post_thumbnail_url( get_the_ID(), 'medium-thumbnail'),
											'alt' => get_the_title(),
											'data-sizes' => 'auto'
										)); ?>
									<span class="video_play_btn">
										<i class="fas fa-film"></i>
									</span>
		                        </a>   
							</figure> 
							<?php else: ?>
								<a href="<?php the_permalink() ?>" title="Link to <?php the_title_attribute(); ?>" class="thumbnail_cnt__link thumbnail_cnt__link--standard">
									<img data-src="<?php echo get_template_directory_uri() . '/images/video_placeholder.png'; ?>" src="<?php echo get_template_directory_uri() . '/images/video_placeholder_small.png'; ?>" data-sizes="auto" class="video_thumbnail lazyload blur-up" alt="<?php the_title(); ?>">
									<span class="video_play_btn">
										<i class="fas fa-film"></i>
									</span>
								</a>
						<?php endif; ?>
						<h2 class="index_post__heading to_animate">
							<a href="<?php the_permalink() ?>" title="Link to <?php the_title_attribute(); ?>">
								<?php the_title(); ?>
							</a>
						</h2>
						<div class="index_post__info to_animate <?php echo ( get_the_excerpt() == '' ) ? 'padding_b' : ''; ?>">
							<div class="index_post_categories">
								<?php
									$categories = get_the_category();
		                            $divider = ', ';
									if ( ! empty( $categories ) ) {
										echo 'Category: ';
										foreach ($categories as $key => $cat) {
											if(count($categories) != ($key + 1 )){
												echo '<a href="' . esc_url( get_category_link( $cat->term_id ) ) . '">' . esc_html( $cat->name ) . '</a>'. $divider; 
											}else{
												echo '<a href="' . esc_url( get_category_link( $cat->term_id ) ) . '">' . esc_html( $cat->name ) . '</a>'; 
											}
										} 
									} 
								 ?>
							</div>
						</div>
						
						<?php if( get_the_excerpt() != '' ): ?>
							<div class="index_post__excerpt to_animate">
								<?php the_excerpt(); ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</article>
		<?php break;

		case 'link':?>
			<article <?php post_class( array('index_post', 'index_link', 'column', 'col-sm-12', 'col-lg-6', 'col-4') ); ?>>
				<div>
					<div class="to_animate link_post">
						<h2 class="index_post__heading">
							<a href="<?php echo wp_strip_all_tags(get_the_content()); ?>" title="Link to <?php the_title_attribute(); ?>" rel="nofollow">
								<?php the_title(); ?>
							</a>
						</h2>
						<div class="link_post__info">
							<div class="index_post_categories">
								<?php
									$categories = get_the_category();
		                            $divider = ', ';
									if ( ! empty( $categories ) ) {
										echo 'Category: ';
										foreach ($categories as $key => $cat) {
											if(count($categories) != ($key + 1 )){
												echo '<a href="' . esc_url( get_category_link( $cat->term_id ) ) . '">' . esc_html( $cat->name ) . '</a>'. $divider; 
											}else{
												echo '<a href="' . esc_url( get_category_link( $cat->term_id ) ) . '">' . esc_html( $cat->name ) . '</a>'; 
											}
										} 
									} 
								 ?>
							</div>
						</div>
						<div class="link_post__icon">
							<a href="<?php echo wp_strip_all_tags(get_the_content()); ?>" title="Link to <?php the_title_attribute(); ?>" rel="nofollow">
								<i class="fas fa-link"></i>
							</a>
						</div>
						
						<div class="link_post__comments">
							<a href="<?php the_permalink() ?>" title="Link to <?php the_title_attribute(); ?>" class="link">
								View comments
							</a>
						</div>
					</div>
				</div>
			</article>
		<?php break;

		case 'quote':?>

			<article <?php post_class( array('index_post', 'quotes_post', 'column', 'col-sm-12', 'col-lg-6', 'col-4') ); ?>>
				<div>
					<div class="to_animate">
						<h2 class="index_post__heading">
							<a href="<?php the_permalink() ?>" title="Link to <?php the_title_attribute(); ?>">
								<?php the_title(); ?>
							</a>
						</h2>
						<div class="index_post__info">
							<div class="index_post_categories">
								<?php
									$categories = get_the_category();
		                            $divider = ', ';
									if ( ! empty( $categories ) ) {
										echo 'Category: ';
										foreach ($categories as $key => $cat) {
											if(count($categories) != ($key + 1 )){
												echo '<a href="' . esc_url( get_category_link( $cat->term_id ) ) . '">' . esc_html( $cat->name ) . '</a>'. $divider; 
											}else{
												echo '<a href="' . esc_url( get_category_link( $cat->term_id ) ) . '">' . esc_html( $cat->name ) . '</a>'; 
											}
										} 
									} 
								 ?>
							</div>
						</div>
						<div class="quotes_post__content">
							<p>
								<a href="<?php the_permalink() ?>" title="Link to <?php the_title_attribute(); ?>">
									<span class="pr-1">
										<i class="fas fa-quote-right"></i>
									</span>
									<?php  echo get_the_excerpt(); ?>
									<span class="pl-1">
										<i class="fas fa-quote-left"></i>
									</span>	
								</a>
							</p>	
						</div>
					</div>
				</div>
			</article>
		<?php break;

		default:?>
			<article <?php post_class( array('index_post', 'column', 'col-sm-12', 'col-lg-6', 'col-4') ); ?>>
				<div>
					<div class="">
						<?php if(has_post_thumbnail()): ?>
							<figure class="thumbnail_cnt">
								<a href="<?php the_permalink() ?>" title="Link to <?php the_title_attribute(); ?>" class="thumbnail_cnt__link">
									<?php the_post_thumbnail('small-thumbnail', 
										$attr = array('class' => "thumbnail lazyload blur-up",
													  'data-src'=> get_the_post_thumbnail_url( get_the_ID(), 'medium-thumbnail'),
													  'alt' => get_the_title(),
													  'data-sizes' => 'auto'
											)); ?>
		                        </a>   
							</figure> 
						<?php endif;?>
						<h2 class="index_post__heading to_animate">
							<a href="<?php the_permalink() ?>" title="Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
						</h2>
						<div class="index_post__info to_animate <?php echo ( get_the_excerpt() == '' ) ? 'padding_b' : ''; ?>">
							<div class="index_post_categories">
								<?php
									$categories = get_the_category();
		                            $divider = ', ';
									if ( ! empty( $categories ) ) {
										echo 'Category: ';
										foreach ($categories as $key => $cat) {
											if(count($categories) != ($key + 1 )){
												echo '<a href="' . esc_url( get_category_link( $cat->term_id ) ) . '">' . esc_html( $cat->name ) . '</a>'. $divider; 
											}else{
												echo '<a href="' . esc_url( get_category_link( $cat->term_id ) ) . '">' . esc_html( $cat->name ) . '</a>'; 
											}
										} 
									} 
								 ?>
							</div>
						</div>
						
						<div class="index_post__excerpt to_animate">
							<?php the_excerpt(); ?>
						</div>
					</div>
				</div>
			</article>
		<?php break;
	}
	