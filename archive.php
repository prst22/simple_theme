<?php get_header(); ?>

	<main class="container grid-xl main archive_page">
        <div class="columns main__inner">
                
		<?php
			if ( have_posts() ) {

				echo '<div class="col-12">';
				echo '<header >';
				
					the_archive_title( '<h1 class="archive_page__title">', '</h1>' );
			
			    echo '</header>';
			    echo '</div>';

				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/one-post', get_post_format() );

				endwhile;
				
				$prev = get_previous_posts_link( '<i class="fas fa-long-arrow-alt-left"></i> prev page ');
				$next = get_next_posts_link( 'next page <i class="fas fa-long-arrow-alt-right"></i>'); 

				if($prev || $next){
					echo '<div class="pagination_links">';
					if($prev){
					    echo $prev;
					}
					if($next){
					    echo $next;
					}
					echo '</div>';
				}

			}else {

				get_template_part( 'template-parts/content', 'none' );

			}
		?>

		</div>
	</main>

<?php
get_footer();