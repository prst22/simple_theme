jQuery(document).ready(function($) {
	$(function() {  
	    $(".mobile_menu_cnt__inner").niceScroll({
	    	cursorcolor: "#1a1a1a",
            autohidemode: false,
            cursorwidth: "5px",
            cursorborderradius: 2,
            zindex: 800
	    });
	});
});
window.addEventListener("DOMContentLoaded", () =>{
  let itemsToAnimate = document.querySelectorAll('.to_animate:not(.revealed)');
  if(itemsToAnimate){
  	let arrayOfItemsToAnimate = Array.from(itemsToAnimate);
  	arrayOfItemsToAnimate.map(hiddenItem => {
  		hiddenItem.classList.add('revealed');
  	});
  }
});

// mobile menu start
let menuBtn = document.getElementById('mobile_menu_btn');
let closeMenuBtn = document.getElementById('close_menu_btn');
let mobileMenu = document.querySelector('.mobile_menu_cnt');
let mobileMenuList = document.querySelector('.mobile_menu_cnt__inner');

if(menuBtn){
	menuBtn.addEventListener('click', toggleMobileMenu);
	closeMenuBtn.addEventListener('click', toggleMobileMenu);
}

if(mobileMenu){
	function controllVpSize(x) {
		if (x.matches && document.body.classList.contains('mobile_menu_active')) { // If media query matches
			toggleMobileMenu(); 
	    } 
	}
  	let x = window.matchMedia("(min-width:  1021px)");
	controllVpSize(x); // Call listener function at run time
	x.addListener(controllVpSize);
	document.addEventListener('keydown', closeMenuOnEsc);
}

function toggleMobileMenu(e){
	function openCloseMenuWithNoTransitionDelay(){
		mobileMenu.classList.remove('transition_delay');
		document.body.classList.toggle('mobile_menu_active');
		mobileMenu.classList.toggle('hidden');
		mobileMenuList.classList.toggle('hidden');
	}
	if(e){ //check event to target click 
		if(e.target.closest('button') === closeMenuBtn 
			|| e.key == "Escape" 
			|| e.key == "Esc" 
			|| e.keyCode == 27){ //choose open or close or esc button to determin whether to use transition delay
			document.body.classList.toggle('mobile_menu_active');
			mobileMenu.classList.add('transition_delay');
			mobileMenu.classList.toggle('hidden');
			mobileMenuList.classList.toggle('hidden');
		}else{
			openCloseMenuWithNoTransitionDelay();
		}
	}else{
		openCloseMenuWithNoTransitionDelay();
	}
	mobileMenu.addEventListener(transitionEvent, ()=>{
		jQuery(document).ready(function($) {
			if(document.body.classList.contains('mobile_menu_active')){
				$('.mobile_menu_cnt__inner').getNiceScroll().resize();
			}
		});
	});
}

function closeMenuOnEsc (evt){
    evt = evt || window.event;
    let isEscape = false;
    if("key" in evt){
        isEscape = (evt.key == "Escape" || evt.key == "Esc");
    }else{
        isEscape = (evt.keyCode == 27);
    }
    if(isEscape && document.body.classList.contains('mobile_menu_active')){
      toggleMobileMenu(evt);
    } 
}
function whichTransitionEvent(){
  let t,
      el = document.createElement("fakeelement");

  let transitions = {
    "transition"      : "transitionend",
    "OTransition"     : "oTransitionEnd",
    "MozTransition"   : "transitionend",
    "WebkitTransition": "webkitTransitionEnd"
  }

  for (t in transitions){
    if (el.style[t] !== undefined){
      return transitions[t];
    }
  }
}

let transitionEvent = whichTransitionEvent();

// mobile menu end
